(function () {
    'use strict';

    function ConvertToNumber(data) {
        return Number(data);
    }

    function getRingColor(percentage) {
        if(percentage > 0 && percentage < 50){
            return "#008000";
        }
        else if(percentage >= 50 && percentage < 75) {
            return "#e8c74a";
        } else {
            return "#cc334a";
        }
    }

    function getPercentage(total, rmpv) {
        return ((total / rmpv) * 100);
    }

    function getLabelText(total, rmpv) {
        return (total + "/" + rmpv);
    }

    function getPageVolumeData(mono, color, rmpv) {
        var data = {};
        if(isNaN(mono) && isNaN(color) && isNaN(rmpv))
        {
            return "Cannot Calculate PageVolume";
        }
        else {
            mono = ConvertToNumber(mono);
            color = ConvertToNumber(color);
            rmpv = ConvertToNumber(rmpv);
            data.total = mono + color;
            data.percent = getPercentage(data.total, rmpv);
            data.labelText = getLabelText(data.total, rmpv);
            data.ringColor = getRingColor(data.percent);
        }
        return data;
    }

    describe("Page Volume", function () {
        it("Mono:80 Color:20 RMPV:80 ", function () {
            expect(getPageVolumeData(40, 20, 80)).toEqual({
                total: 60,
                percent: 75,
                labelText: "60/80",
                ringColor: "#cc334a"
            })
        });
        it("Mono:50 Color:50 RMPV:200 ", function () {
            expect(getPageVolumeData(50, 50, 200)).toEqual({
                total: 100,
                percent: 50,
                labelText: "100/200",
                ringColor: "#e8c74a"
            })
        });
        it("Mono:20 Color:50 RMPV:1200 ", function () {
            expect(getPageVolumeData(70, 50, 1200)).toEqual({
                total: 120,
                percent: 10,
                labelText: "120/1200",
                ringColor: "#008000"
            })
        });
        it("Mono:'50' Color:'50' RMPV:'200' ", function () {
            expect(getPageVolumeData("50", "50", "200")).toEqual({
                total: 100,
                percent: 50,
                labelText: "100/200",
                ringColor: "#e8c74a"
            })
        });
        it("Mono:a Color:b RMPV:c ", function () {
            expect(getPageVolumeData("a", "b", "c")).toEqual("Cannot Calculate PageVolume"
            )
        });
    });
}());