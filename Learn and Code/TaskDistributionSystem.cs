using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskDistributionSystem
{
    public class Client
    {
        int taskIDs[];
        String destinationIP;

        public void getResult();
        public void displayResult();
        public void sendRequest();
    }

    public class Cordinator
    {
        Queue<Task> taskQueue = new Queue<Task>();
        String ipAddresses[];
        Boolean isNodeAvailable;

        public void registerNodes();
        public void enqueueTasks();
        public void dequeueTasks();
        public void sendRequests();
        public void recieveRequests();
        public void retrieveFromDB();
    }


    public class Node
    {
        int taskID;

        public void receiveRequest();
        public void sendAck();
        public void executeTask();
        public void sendResult();
    }

    public class DbConnections
    {
        Connection con;
        String message;

        public void connect();
        public void disconnect();
    }

}
