using Spiders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Spiders
{
    class Spider
    {
        public int position;
        public int power;
    }

    class OperationsOnSpiders
    {

        public static void LoadSpiders(int totalNumberOfSpiders, int selectedSpiders, Queue<Spider>  spiderQueue)
        {
            if(selectedSpiders>=1 && selectedSpiders<=316 && totalNumberOfSpiders>=selectedSpiders && totalNumberOfSpiders<=(selectedSpiders*selectedSpiders))
            {
                int indexOfPower = 1;
                string[] inputSpiderPower = Console.ReadLine().Split();
                int[] powerOfSpiders = Array.ConvertAll(inputSpiderPower, int.Parse);
                foreach (var power in powerOfSpiders)
                {
                    Spider obj = new Spider();
                    obj.power = power;
                    obj.position = indexOfPower++;
                    spiderQueue.Enqueue(obj);
                }
            }
            else
            {
                Console.WriteLine("Enter the Values within the range\n\t1 <= X <= 316\n\tX <= N <= X*X\nwhere X ---> No.of spiders selected\n\tN ---> Total number of spiders");
                Console.Read();
                System.Environment.Exit(0);
            }

        }

        public static void DeQueueSpiders(int selectedSpiders, Queue<Spider> spiderQueue, List<Spider> spiderList)
        {
            for (int i = 0; i < selectedSpiders; i++)
            {
                if (spiderQueue.Count == 0)
                    return;
                spiderList.Add(spiderQueue.Dequeue());
            }

        }

        public static void FindMaximumPower(List<Spider> SpiderList)
        {
            Spider MaximumPowerSpider = SpiderList.First<Spider>(); 
            foreach (Spider spider in SpiderList)
            {
                if (MaximumPowerSpider.power == spider.power)
                {
                    continue;
                }
                if (MaximumPowerSpider.power < spider.power)
                {
                    MaximumPowerSpider = spider;
                }

            }
            Console.Write(MaximumPowerSpider.position + " ");
            SpiderList.Remove(MaximumPowerSpider);
        }

        public static void ReEnQueueSelectedSpiders(List<Spider> SpiderList, Queue<Spider> spiderQueue)
        {
            foreach (Spider spider in SpiderList)
            {
                if (spider.power != 0)
                {
                    spider.power--;
                }
                spiderQueue.Enqueue(spider);
            }
        }

    }

    class MonkAndChamberOfSecret
    {
        static void Main(string[] args)
        {
            int totalNumberOfSpiders, selectedSpiders;
            Queue<Spider> spiderQueue = new Queue<Spider>();
            List<Spider> spiderList = new List<Spider>();
            string[] userInputLine = Console.ReadLine().Split();
            Int32.TryParse(userInputLine[0], out totalNumberOfSpiders);
            Int32.TryParse(userInputLine[1], out selectedSpiders);

            OperationsOnSpiders.LoadSpiders(totalNumberOfSpiders, selectedSpiders, spiderQueue);

            for (int i = 0; i < selectedSpiders; i++)
            {
                OperationsOnSpiders.DeQueueSpiders(selectedSpiders, spiderQueue, spiderList);
                OperationsOnSpiders.FindMaximumPower(spiderList);
                OperationsOnSpiders.ReEnQueueSelectedSpiders(spiderList, spiderQueue);
                spiderList.Clear();
            }
            Console.ReadLine();
        }
    }
}

    