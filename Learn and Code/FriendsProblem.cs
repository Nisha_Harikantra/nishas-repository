using System;
using System.Collections.Generic;
using System.Linq;


namespace FriendsProblem
{
    class RemoveFriendsProblem
    {
        static void Main(string[] args)
        {
            Stack<int> friendList = new Stack<int>();
            int totalFriends = 0, friendsToBeDeleted = 0;
            int TestCases = int.Parse(Console.ReadLine());

            if (TestCases >= 1 && TestCases <= 1000)
            {
                for (int i = 0; i < TestCases; i++)
                {
                    friendList.Clear();
                    string[] inputString = Console.ReadLine().Split(' ');
                    int.TryParse(inputString[0], out totalFriends);
                    int.TryParse(inputString[1], out friendsToBeDeleted);

                    if (totalFriends >= 1 && totalFriends <= 100000 && friendsToBeDeleted >= 0 && friendsToBeDeleted < totalFriends)
                    {
                        DeleteFriendsFromList(friendList, totalFriends, friendsToBeDeleted);
                        DisplayFriendList(friendList);
                    }
                }
            }
            Console.Read();
        }

        public static void DeleteFriendsFromList(Stack<int> friendList, int totalFriends, int friendsToBeDeleted)
        {
            string[] popularitiesOfFriends = Console.ReadLine().Split(' ');
            int countOfPopedItem = 0;
            for (int index = 0; index < totalFriends; index++)
            {
                int popularity = int.Parse(popularitiesOfFriends[index]);
                if (popularity >= 0 && popularity <= 100)
                {
                    while (countOfPopedItem < friendsToBeDeleted && friendList.Count > 0 && friendList.Peek() < popularity)
                    {
                        friendList.Pop();
                        countOfPopedItem++;
                    }
                    friendList.Push(popularity);
                }
            }
        }

        public static void DisplayFriendList(Stack<int> friendList)
        {
            foreach(var friend in friendList.Reverse())
            {
                Console.WriteLine(friend + " ");
            }
            Console.WriteLine();
        }
    }
}