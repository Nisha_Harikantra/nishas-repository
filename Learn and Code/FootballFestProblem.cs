using System;

public class FootBallFestProblem
{
    public static void Main(string[] args)
    {
        try
        {
            int testCases = Convert.ToInt32(Console.ReadLine());

            while (testCases > 0 && testCases <= 100)
            {
                string numberOfPassesAndPlayerID = Console.ReadLine();
                int numberOfPasses, currentPlayerID = 0, previousPlayerID = 0;
                string[] arrayOfPassesAndID = numberOfPassesAndPlayerID.Split(' ');
                Int32.TryParse(arrayOfPassesAndID[0], out numberOfPasses);
                Int32.TryParse(arrayOfPassesAndID[1], out currentPlayerID);

                while (numberOfPasses > 0 && numberOfPasses <= 100000 && currentPlayerID > 0 && currentPlayerID <= 1000000)
                {
                    string passesAndID = Console.ReadLine();
                    string[] passtypeAndID = passesAndID.Split(' ');
                    if (passtypeAndID[0] == "B" || passtypeAndID[0] == "b")
                    {
                        int temporarypPlayerID = currentPlayerID;
                        currentPlayerID = previousPlayerID;
                        previousPlayerID = temporarypPlayerID;
                    }

                    else if (passtypeAndID[0] == "P" || passtypeAndID[0] == "p")
                    {
                        previousPlayerID = currentPlayerID;
                        Int32.TryParse(passtypeAndID[1].ToString(), out currentPlayerID);
                    }
                    else
                    {
                        Console.WriteLine("Invalid PassType");
                    }

                    numberOfPasses--;
                }
                Console.WriteLine("Player {0}", currentPlayerID);
                testCases--;
            }
        }
        catch (IndexOutOfRangeException)
        {
            Console.WriteLine("please enter the number of passes and player ID as \"P 23\"");
        }
        Console.ReadKey();
    }
}